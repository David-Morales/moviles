package ec.edu.epn.morales.david.timefighter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class ResultsRecyclerViewHolder(item: View): RecyclerView.ViewHolder(item){
    val namesListResults= itemView.findViewById<TextView>(R.id.item_name)
    val scoreListResults=itemView.findViewById<TextView>(R.id.item_score)
}