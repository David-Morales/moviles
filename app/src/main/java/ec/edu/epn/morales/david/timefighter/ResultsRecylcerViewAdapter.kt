package ec.edu.epn.morales.david.timefighter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference

class ResultsRecylcerViewAdapter(ref: DatabaseReference):
    RecyclerView.Adapter<ResultsRecyclerViewHolder>(){

    var resultsList: MutableList<ResultList> = mutableListOf()

    init {
        ref.addChildEventListener(object: ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val name= item.child("name").value.toString()
                val listId= item.key.toString()
                val score= item.child("score").value.toString()

                resultsList.add(ResultList(listId,name,score))

                notifyItemInserted(resultsList.size)
            }

            override fun onChildRemoved(item: DataSnapshot) {

            }

        })
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsRecyclerViewHolder {
        val view= LayoutInflater.from(parent.context)
                .inflate(R.layout.results_view_holder,parent,false)

        return ResultsRecyclerViewHolder(view)

    }

    override fun getItemCount(): Int {
        return resultsList.count()
    }

    override fun onBindViewHolder(holder: ResultsRecyclerViewHolder, position: Int) {
        holder.namesListResults.text=resultsList[position].name
        holder.scoreListResults.text=resultsList[position].score
    }

}