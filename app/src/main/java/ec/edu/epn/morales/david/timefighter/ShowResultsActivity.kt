package ec.edu.epn.morales.david.timefighter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase

class ShowResultsActivity : AppCompatActivity() {

    internal lateinit var showResultsView : RecyclerView

    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("");

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_results)

        showResultsView= findViewById(R.id.show_results_view)
        showResultsView.layoutManager=LinearLayoutManager(this)
        showResultsView.adapter=ResultsRecylcerViewAdapter(ref)
    }
}
