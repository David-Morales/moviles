package ec.edu.epn.morales.david.timefighter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import java.util.*


class GameMainActivity : AppCompatActivity() {

    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: View
    internal lateinit var showResultsButton: Button

    internal var score =0
    val database = FirebaseDatabase.getInstance().getReference()

    internal var gameStarted = false
    internal lateinit var countDownTimer: CountDownTimer
    internal val countDownIntervval = 1000L
    internal val initialCountDown= 60000L
    internal var timeLeft=60

    internal val TAG = GameMainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)
        Log.d(TAG, "onCreate called . Score is $score")



        //connect views and variables
        gameScoreTextView = findViewById<TextView>(R.id.game_score_text_view)
        timeLeftTextView = findViewById<TextView>(R.id.time_left_text_view)
        tapMeButton = findViewById<View>(R.id.tap_button)
        showResultsButton = findViewById(R.id.show_results)

        if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }

        tapMeButton.setOnClickListener{ _->
            incrementScore()

        }

        showResultsButton.setOnClickListener{_->
            showResults()
        }

    }

    private fun resetGame(){
        score=0
        timeLeft= 60
        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text=gameScore
        val timeLeftText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text=timeLeftText

        countDownTimer= object : CountDownTimer(initialCountDown,countDownIntervval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft= millisUntilFinished.toInt()/1000
                timeLeftTextView.text= getString(R.string.time_left, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }

        gameStarted=false


    }


    private fun incrementScore(){
        score ++

        //val newScore= "Your score: " + Integer.toString(score)
        val newScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = newScore

        if(!gameStarted){
            startGame()
        }

    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted= true

    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over_message,Integer.toString(score)),Toast.LENGTH_LONG).show()
        showAddResult(score)
        resetGame()

    }

    fun restoreGame() {
        val restoredGame= getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restoredGame

        val restoredTime= getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text = restoredTime


        countDownTimer = object : CountDownTimer(timeLeft*1000L, countDownIntervval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft= millisUntilFinished.toInt()/1000

                timeLeftTextView.text = getString(R.string.time_left, timeLeft.toString())
            }

            override fun onFinish() {
                endGame()
            }
        }

        countDownTimer.start()
        gameStarted = true
    }



    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY,score)
        outState!!.putInt(TIME_LEFT_KEY,timeLeft)
        countDownTimer.cancel()

        Log.d(TAG, "onSsveInstancesState: score = $score & timeLeft: $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "onDestroy called")
    }

    private fun showAddResult(gotScore:Int){
        val dialogTitle=getString(R.string.result_dialog_title)
        val positiveButtonTitle = getString(R.string.create_register)


        val builder = AlertDialog.Builder(this)
        val nameEditText = EditText(this)
        nameEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(dialogTitle)
        builder.setView(nameEditText)


        builder.setPositiveButton(positiveButtonTitle) { dialog, i ->
            val register=Register()
            register.name= nameEditText.text.toString()
            register.score=gotScore

            val newId = UUID.randomUUID().toString()
            database.child(newId).
                    setValue(register)

            dialog.dismiss()
        }


        builder.create().show()
    }

    private fun showResults(){
        val intent = Intent(this, ShowResultsActivity::class.java)

        startActivity(intent)

    }
//wef
}

